# MileApp Ads Page Form Testing

This guide demonstrates how to use Selenium to perform form testing on the MileApp ads page.

## Prerequisites

Before you begin, make sure you have the following installed:

- Python: https://www.python.org/downloads/
- Selenium: You can install it using `pip`:
  `pip install selenium`
- Make sure the selenium version is above 4.3.0 you can check the version by `pip freeze` and see the version of selenium in the list
- Appropriate WebDriver: ChromeDriver is already provided in this project repo. So if you use Chrome browser, you dont need to install any webdriver

## Getting Started

1. Clone or download this repository to your local machine.

2. Open a terminal and navigate to the directory containing the cloned/downloaded files.

3. Customize the `config.json` script:

- Replace the example URLs with the URLs of the web pages you want to test by changing the value of `urls` in the `json`.
- You can change the field you want to insert in `fields` value in the `json`.
- Change the iteration of testing for each pages by changing the value of `iterations` in the `json`.

4. Run the `script.py` script:
   `python script.py`

## Output

![Alt text](image.png)

The expected output should be similar to the image above. It contains the success rate of each pages form input.

Happy testing!
