from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import json
import time
import requests

with open('config.json') as config_file:
    config = json.load(config_file)

urls = config['urls']
fields = config['fields']
iterations = config['iterations']
result = ''

driver = webdriver.Chrome()

for url in urls:
    fails = 0
    for i in range(iterations):
        driver.get(url)
        time.sleep(1)
        full_name_input = driver.find_element("id", "full_name")
        phone_input = driver.find_element("id", "phone")
        email_input = driver.find_element("id", "email")
        company_input = driver.find_element("id", "company")
        company_size_input = driver.find_element("id", "company_size")

        full_name_input.send_keys(fields["full_name"] + " " + str(i))
        phone_input.send_keys(fields["phone"])
        email_input.send_keys(fields["email"])
        company_input.send_keys(fields["company"])
        select = Select(company_size_input)
        
        if (fields["company_size"] == 1):
            select.select_by_visible_text("1-100")
        elif (fields["company_size"] == 2):
            select.select_by_visible_text("101-1000")
        else:
            select.select_by_visible_text("1001+")

        driver.find_element("xpath", "//button[@type='submit']").send_keys(Keys.ENTER)
        time.sleep(2)

        current_url = driver.current_url
        expected_url = "https://mile.app/thank-you"
        if current_url != expected_url:
            fails += 1

        time.sleep(2)
    result += "Success rate for " + url + " page is: " + str((1 - (fails/iterations)) * 100) + " percent.\n"

driver.quit()

# Test result
print()
print("Test result:")
print(result)

# Send test result to Coda
url = "https://coda.io/apis/v1/docs/JVeV9wZCQ1/tables/grid-oNvuAt-rJE/rows"

headers = {
    "Authorization": "Bearer 940cfdf8-9d53-41a7-8517-acb68119d0a2",
    "Content-Type": "application/json"
}

data = {
    "rows": [
        {
            "cells": [
                {
                    "column": "c-cHi__nyt71",
                    "value": "2023-11-11"
                },
                {
                    "column": "c-dMMUFikRjG",
                    "value": url
                },
                {
                    "column": "c-NafVGV_VYO",
                    "value": result
                }
            ]
        }
    ],
    "keyColumns": [
        "c-cHi__nyt71"
    ]
}

# Convert the data dictionary to a JSON string
data_json = json.dumps(data)

# Send the POST request
response = requests.post(url, headers=headers, data=data_json)

# Check the response
if response.status_code // 100 == 2:
    print("Request successful")
    print("Response:", response.json())
else:
    print("Request failed with status code:", response.status_code)
    print("Response:", response.text)